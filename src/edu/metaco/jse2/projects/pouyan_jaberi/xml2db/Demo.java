package edu.metaco.jse2.projects.pouyan_jaberi.xml2db;
import edu.metaco.jse2.projects.pouyan_jaberi.xml2db.ctrl.BeanXMLConverter;
import edu.metaco.jse2.projects.pouyan_jaberi.xml2db.model.PersonDTO;
import java.io.IOException;

/**
 * Created by Pouyan on 7/16/2017.
 */
public class Demo {
    public static void main(String[] args) {
        PersonDTO person = new PersonDTO(0, "Pouyan", "01001010", 21, 19960618);
        /*try {
            System.out.println("This: ");
            BeanXMLConverter.write2XML(person);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        try {
            //BeanXMLConverter.serializer(person);
            PersonDTO personDTO = (PersonDTO) BeanXMLConverter.deserializer(BeanXMLConverter.defaultStorageFile);
            System.out.println(personDTO.getId() + " " + personDTO.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
