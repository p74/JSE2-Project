package edu.metaco.jse2.projects.pouyan_jaberi.xml2db.ctrl;

//<editor-fold desc="imported libs">
import edu.metaco.jse2.projects.pouyan_jaberi.xml2db.model.PersonDTO;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.List;
//</editor-fold>

/**
 * Created by Pouyan on 7/12/2017.
 */

public class BeanXMLConverter {
    public static final String defaultXMLFile = "src\\edu\\metaco\\jse2\\projects\\pouyan_jaberi\\xml2db\\person.xml";
    public static final String defaultStorageFile = "D:\\person";
    private static FileInputStream fileInputStream;
    private static FileOutputStream fileOutputStream;
    private static ObjectOutputStream objectOutputStream;
    private static ObjectInputStream objectInputStream;

    /**
     * This method converts a PersonDTO POJO to an XML file and writes it on the defaultXMLFile.
     * @param person gets an PersonDTO object.
     * @throws IOException
     */
    public static void write2XML(PersonDTO person) throws IOException {
        Document document = DocumentHelper.createDocument();
        Element personElement = document.addElement("person");

        Element nameElement = personElement.addElement("name");
        nameElement.addText(person.getName());
        Element familyElement = personElement.addElement("family");
        familyElement.addText(person.getFamily());
        Element ageElement = personElement.addElement("age");
        ageElement.addText(String.valueOf(person.getAge()));
        Element birthdateElement = personElement.addElement("birthdate");
        birthdateElement.addText(String.valueOf(person.getBirthdate()));

        OutputFormat outputFormat = OutputFormat.createCompactFormat();
        FileWriter fileWriter = new
                FileWriter(defaultXMLFile);
        XMLWriter xmlWriter = new XMLWriter(fileWriter, outputFormat);
        xmlWriter.write(document);
        fileWriter.close();
    }

    /**
     * This method converts a PersonTo POJO to an XML file at the given path.
     * @param person gets and PersonTo object to convert to an XML doc.
     * @param path gets the path to save the XML file.
     * @throws IOException
     */
    public static void write2XML (PersonDTO person, String path) throws IOException {
        Document document = DocumentHelper.createDocument();
        Element personElement = document.addElement("person");

        Element nameElement = personElement.addElement("name");
        nameElement.addText(person.getName());
        Element familyElement = personElement.addElement("family");
        familyElement.addText(person.getFamily());
        Element ageElement = personElement.addElement("age");
        ageElement.addText(String.valueOf(person.getAge()));
        Element birthdateElement = personElement.addElement("birthdate");
        birthdateElement.addText(String.valueOf(person.getBirthdate()));

        OutputFormat outputFormat = OutputFormat.createCompactFormat();
        FileWriter fileWriter = new
                FileWriter(path);
        XMLWriter xmlWriter = new XMLWriter(fileWriter, outputFormat);
        xmlWriter.write(document);
        fileWriter.close();
    }

    /**
     * This method converts a PersonTo POJO to an XML doc.
     * @param person gets and PersonTo object to convert to an XML doc.
     * @return the XML doc.
     * @throws IOException
     */
    public static Document getXMLDoc (PersonDTO person) throws IOException {
        Document document = DocumentHelper.createDocument();
        Element personElement = document.addElement("person");

        Element nameElement = personElement.addElement("name");
        nameElement.addText(person.getName());
        Element familyElement = personElement.addElement("family");
        familyElement.addText(person.getFamily());
        Element ageElement = personElement.addElement("age");
        ageElement.addText(String.valueOf(person.getAge()));
        Element birthdateElement = personElement.addElement("birthdate");
        birthdateElement.addText(String.valueOf(person.getBirthdate()));

        return document;
    }

    public static void serializer (PersonDTO person) throws IOException {
        fileOutputStream = new FileOutputStream(defaultStorageFile);
        objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(person);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    public static Object deserializer (String storageFile) throws IOException, ClassNotFoundException {
        fileInputStream = new FileInputStream(defaultStorageFile);
        objectInputStream = new ObjectInputStream(fileInputStream);
        Object object = objectInputStream.readObject();

        return object;
    }

}
