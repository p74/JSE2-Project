package edu.metaco.jse2.projects.pouyan_jaberi.xml2db.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Pouyan on 7/11/2017.
 */

public class PersonDTO implements Serializable {
    private String name;
    private String family;
    private int age;
    private int birthdate;
    private int id;

    //<editor-fold desc="Getters & Setters">
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(int birthdate) {
        this.birthdate = birthdate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    //</editor-fold>


    public PersonDTO() { }

    public PersonDTO(int id, String name, String family, int age, int birthdate) {
        this.name = name;
        this.family = family;
        this.age = age;
        this.birthdate = birthdate;
        //this.id  = id;
    }
}
